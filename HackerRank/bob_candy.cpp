#include <iostream>
using namespace std;
int dollar_bills[7]  = {1,2,5,10,20,50,100};

int no_of_ways ( int N , int i)
{
  //Subtracted Too Much or Used all types of Dollar Bills
  if ( N < 0 || i == 7)
    return 0;

  //Paid N Dollars
  if ( N == 0 )
    return 1;


  int answer = 0;
  int dollar_bill = dollar_bills[i];

  int k = N / dollar_bill;

  //All Consecutive Dollars are of higher denomination
  if ( k == 0 )
    return 0;

  for (size_t no_of_times = 0; no_of_times <= k; no_of_times++)
    answer += no_of_ways(N - dollar_bill*no_of_times, i+1);
  //cout << N << ":" << i << ":" << answer << endl;
  return answer;
}

int main(int argc, char const *argv[])
{
  int N;
  cin >> N;
  cout << no_of_ways(N , 0);

}
