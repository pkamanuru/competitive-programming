#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
long long answer[10000001];
long long no_of_computations(long long N)
{
        for ( int i=2; i<=N; i++ )
        {
                if ( answer[i] != -1 )
                        continue;
                else
                {
                        if ( i % 2 == 0 )
                                answer[i] = answer[i/2-1]+answer[i/2]+i;
                        else
                                answer[i] = 2*answer[i/2] + i;
                }
        }
        return answer[N];
}
int main() 
{
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
        int N=-1,old_N=-1;
        fill_n(answer,10000001,-1);
        answer[0]=0;
        answer[1]=1;
        int i=0;
        while ( 1 )
        {
                old_N = N;
               cin >> N;
               if ( old_N == N )
                       break;
               i++;
               cout << "Case "<< i << ": " << no_of_computations(N) << endl;
       }
    return 0;
}

