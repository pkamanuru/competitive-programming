#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int oppDirDist(int i, int j) {
  if(i > j) {
    swap(i,j);
  }

  //One Key Going to 0
  //One Key Going to 9
  //One extra rotation for either to become the other
  return i + (9-j) + 1;
}

int main() {
  int nbOfDials = 5;
  int firstComb[nbOfDials];
  int secondComb[nbOfDials];

  for(int i=0; i<nbOfDials; i++)
    cin >> firstComb[i];

  for(int i=0; i<nbOfDials; i++)
    cin >> secondComb[i];

  int minNoOfOps = 0;
  for(int i=0; i<nbOfDials; i++) {
    minNoOfOps += min(abs(firstComb[i] - secondComb[i]),
                    oppDirDist(firstComb[i], secondComb[i]));
  }
  cout << minNoOfOps << endl;
  return 0;
}
