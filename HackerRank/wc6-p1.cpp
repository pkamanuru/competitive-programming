#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
  int N,K;
  cin >> N >> K;

  int sum = 0;
  int costOfItemK = 0;
  for(int index=0; index<N; index++) {
    int indexItemCost;
    cin >> indexItemCost;

    if (index == K) {
      costOfItemK = indexItemCost;
    }

    sum += indexItemCost;
  }
  int totalAmountCharged=0;
  cin >> totalAmountCharged;
  if ((sum - costOfItemK)/2 == totalAmountCharged) {
    cout << "Bon Appetit\n";
  } else {
    cout << totalAmountCharged - (sum - costOfItemK)/2 << endl;
  }
  return 0;
}
