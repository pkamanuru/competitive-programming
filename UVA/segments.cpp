#include <iostream>
#include <stack>

using namespace std;

#define MAX_K 10
#define row_length(k) 2*k+3
#define col_length(k) k+2

char output[2*MAX_K+3][MAX_K+2];

//Returns a Stack of Digits
stack <int> stack_of_digits(int no)
{
  stack <int> digits;
  if ( no == 0 )
    digits.push(no);
  while ( no > 0 )
  {
    digits.push(no%10);
    no /= 10;
  }
  return digits;
}

struct coordinates_segment{
  int x_start;
  int x_end;
  int y_start;
  int y_end;
};


char get_character_for_segment ( int segment_no )
{
  segment_no += 1;
  if ( segment_no == 2 || segment_no == 4 || segment_no == 7 )
    return '-';
  else
    return '|';
}

bool segments[10][7]=
{ {true, true, true, false, true, true, true}, //0
  {false, false, true, false, false, true, false}, //1
  {false, true, true, true, true, false, true}, //2
  {false, true, true, true, false, true, true}, //3
  {true, false, true, true, false, true, false}, //4
  {true, true, false, true, false, true, true}, //5
  {true, true, false, true, true, true, true}, //6
  {false, true, true, false, false, true, false}, //7
  {true, true, true, true, true, true, true}, //8
  {true, true, true, true, false, true, true}, //9
};

struct coordinates_segment get_coordinates_of_segment( int size, int segment, int i, int j )
{
  struct coordinates_segment coordinates;
  segment += 1;
  switch ( segment )
  {
  case 1:
    coordinates.x_start = 1;    coordinates.x_end = size;
    coordinates.y_start = 0;      coordinates.y_end = 0;
    break;
  case 2:
    coordinates.x_start = 0;    coordinates.x_end = 0;
    coordinates.y_start = 1;      coordinates.y_end = size;
    break;
  case 3:
    coordinates.x_start = 1;    coordinates.x_end = size;
    coordinates.y_start = size+1;      coordinates.y_end = size+1;
    break;
  case 4:
    coordinates.x_start = size+1;    coordinates.x_end = size+1;
    coordinates.y_start = 1;      coordinates.y_end = size;
    break;
  case 5:
    coordinates.x_start = size+2;    coordinates.x_end = 2*size+1;
    coordinates.y_start = 0;      coordinates.y_end = 0;
    break;
  case 6:
    coordinates.x_start = size+2;    coordinates.x_end = 2*size+1;
    coordinates.y_start = size+1;      coordinates.y_end = size+1;
    break;
  case 7:
    coordinates.x_start = 2*size+2;    coordinates.x_end = 2*size+2;
    coordinates.y_start = 1;      coordinates.y_end = size;
    break;
  default:
    cout << "Invalid Input\n";
    break;
  }
  coordinates.x_start += i; coordinates.x_end += i;
  coordinates.y_start += j; coordinates.y_end += j;

  return coordinates;

}

stack <int> stack_of_digits_string(string no)
{
  stack <int> answer;
  for ( int i=no.length()-1; i>=0; i--)
    answer.push(no.at(i)-'0');
  return answer;
}
void print_stack(stack <int> a)
{
  while ( !a.empty() )
    {
      cout << a.top();
      a.pop();
    }
}

int main(int argc, char const *argv[])
{
  int size; string no;
  cin >> size >> no;
  while ( size > 0 )
  {
    //cout << size << ":" << no << endl;
    //Initialization
    stack <int> digits = stack_of_digits_string(no);
    //print_stack(digits);
    //return 0;
    //char number[row_length(size)][col_length(size)];
    int no_of_digits = digits.size();
    char numbers[no_of_digits][row_length(size)][col_length(size)];

    //Initializing Array
    //We have no_of_digits digits to print
    for ( int digit_no=0; digit_no < no_of_digits; digit_no++ )
    {
      for ( int row=0; row <row_length(size); row++ )
      {
        for ( int col=0; col<col_length(size); col++ )
        {
          numbers[digit_no][row][col] = ' ';
        }
      }
    }


    //Creating a character array for each digit
    while ( ! digits.empty() )
    {
      for ( int digit_no=0; digit_no < no_of_digits; digit_no++ )
      {
        int cur_digit = digits.top(); digits.pop();
        //Cleaning Input Array


          for ( int segment_no = 0; segment_no < 7; segment_no++ )
          {
              if ( segments[cur_digit][segment_no] )
              {
                //Get | or _ based on segment
                char c = get_character_for_segment(segment_no);
                struct coordinates_segment cur_no = get_coordinates_of_segment(size, segment_no, 0, 0);
                //Assigning Corresponding Locations
                for ( int row=cur_no.x_start; row <=cur_no.x_end; row++ )
                {
                  for ( int col=cur_no.y_start; col<=cur_no.y_end; col++ )
                  {
                    numbers[digit_no][row][col] = c;
                  }
                }
              }
          }
      }
    }

    //Printing Final Result
    for ( int row=0; row <row_length(size); row++ )
    {
      for ( int digit_no=0; digit_no < no_of_digits; digit_no++ )
      {
        for ( int col=0; col<col_length(size); col++ )
        {
          cout << numbers[digit_no][row][col];
        }
        if ( digit_no != no_of_digits - 1)
          cout << " ";
      }
        cout << endl;
    }
    cin >> size >> no;
    cout << endl;
  }
  return 0;
}
