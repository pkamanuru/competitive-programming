#include <iostream>
using namespace std;
int main()
{
    int T;
    cin >> T;
    while (T--)
    {
        long long int N; cin >> N;
        N = N*(3*N+1);
        N/=2;
        N%=1000007;
        cout << N <<"\n";
    }
    return 0;
}    
