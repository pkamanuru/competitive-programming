#include <iostream>
#include <string>
using namespace std;
string A,B;
int D[3000][3000];
int smallest(int x, int y, int z)
{
    return std::min(std::min(x, y), z);
}
int main()
{
    int T;
    cin >> T;
    while(T--)
    {
        cin >> A >> B;
        D[0][0]=0;


        for (int i = 1; i <= A.length(); i++) 
            D[i][0]=i;
        for ( int i=1; i<= B.length(); i++ )
            D[0][i]=i;

        for ( int i=1; i<=A.length(); i++ )
        {
            for (int j=1; j <= B.length(); j++) 
            {
                if ( A.at(i-1) == B.at(j-1) )
                      D[i][j] = D[i-1][j-1];
                else
                    D[i][j] = smallest(D[i-1][j],D[i][j-1],D[i-1][j-1])+1;
            }
        }
        cout << D[A.length()][B.length()] << "\n";
    }
    return 0;
}    
