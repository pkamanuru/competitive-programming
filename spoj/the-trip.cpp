#include <iostream>
#include <algorithm>
#include <iomanip>
#include <cstdio>
using namespace std;
int main()
{
        int N; cin >> N;
        while ( N > 0  )
        {
                int expenditure_dollar[N];
                int expenditure_cents[N];
                fill_n(expenditure_dollar,N,0);
                fill_n(expenditure_cents,N,0);
                long long sum=0;
                for (int i=0; i<N; i++)
                {
                        scanf("%d.%d",expenditure_dollar+i,expenditure_cents+i);
                        sum += 100*expenditure_dollar[i] + expenditure_cents[i];
                }
                long long avg = sum / N; 
                long long remaining_cents=sum % N;
                //cout << avg <<":"  << remaining_cents << endl;
                int no_a=0,no_b=0; 
                //All Over Spended Members
                long long a=0,b=0;
                for ( int i=0; i<N; i++ )
                        if ( avg >= 100*expenditure_dollar[i]+expenditure_cents[i])
                        {
                                a+= avg -(100*expenditure_dollar[i]+expenditure_cents[i]);
                                no_a++;
                        }
                        else
                        {
                                b+= 100*expenditure_dollar[i]+expenditure_cents[i] - avg;
                                no_b++;
                        }
                //cout << no_a << ":" << no_b << endl;
                //cout << a << ":" << b << endl;
                //B needs to get extra
                //A Needs to pay extra
                //Let B bear the first Extra Cents and Then A Bear one by one in the extra cents
                //If B pays they will recieve less
                //If A pays they will pay more hence we add the cents
                if ( no_b > remaining_cents )
                        b -= remaining_cents;
                else
                {
                        b -= no_b;
                        remaining_cents -= no_b;
                        a += remaining_cents;
                }

                double ans = min(a,b);
                ans /= double(100);
                printf("$%.2f\n",ans);
                cin >> N;
        }
        return 0;
}        
