#include <iostream>
#include <stack>
#include <algorithm>
using namespace std;

struct node
{
    struct AdjList *list;
    bool is_visited;
    int dest;
    int len1;
    int len2;
};

struct AdjListNode
{
    struct node *node_content;
    struct AdjListNode* next;
};

// A structure to represent an adjacency liat
struct AdjList
{
    struct AdjListNode *head;  // pointer to head node of list
    struct AdjListNode *tail;
    struct AdjListNode *next;
};

// A utility function to create a new adjacency list node
struct AdjListNode* newAdjListNode()
{
    struct AdjListNode* newNode = (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
    newNode->node_content = NULL;
    newNode->next = NULL;
    return newNode;
}
void update_l1_l2(struct node *n, int a)
{
    //cout << "Node :"<<n->dest<<a<<"\n";
    if ( a > n->len1 )
    {
        int temp = n->len1;
        n->len1 = a;
        a=temp;
    }
    if (  a > n->len2 )
        n->len2 = a;
}
class tree 
{
    public:
        int vertices;
        struct node *tree_nodes;
        tree(int n)
        {
            vertices=n; 
            tree_nodes = (struct node *) malloc ( sizeof(struct node)*n );
            for ( int i=0; i<n; i++ )
            {
                tree_nodes[i].dest=i;
                tree_nodes[i].is_visited=false;
                tree_nodes[i].len1=0;
                tree_nodes[i].len2=0;
                tree_nodes[i].list=(struct AdjList *) malloc ( sizeof(struct AdjList) );
                tree_nodes[i].list->head = NULL;
                tree_nodes[i].list->tail = NULL;
                tree_nodes[i].list->next = NULL;
            } 
        }
        int find_highest()
        {
            int max=0;
            for ( int i=0; i<vertices; i++  )
            {
                int sum = tree_nodes[i].len1 + tree_nodes[i].len2;
                if (sum > max )
                    max = sum;
            }
            return max;
        }
        void add_edge(int a , int b)
        {
            //New Node Initialization
            struct AdjListNode *node = ( struct AdjListNode *) malloc ( sizeof(struct AdjListNode) );
            //Node pointing to bth position
            node->node_content = tree_nodes+b;
            node->next = NULL;
            if ( tree_nodes[a].list->tail == NULL )
            {
                tree_nodes[a].list->tail = node;
                tree_nodes[a].list->head = tree_nodes[a].list->tail;
                tree_nodes[a].list->next = tree_nodes[a].list->head;
            }
            else
            {
                tree_nodes[a].list->tail->next = node;
                tree_nodes[a].list->tail = node;
            }
        }

        int find_longest_path()
        {
            stack <int> vertices;
            int cur=0;
            vertices.push(cur);
            do
            {
                   // cout << cur << ";";
                    //Get a Node not visited
                    tree_nodes[cur].is_visited = true;
                    struct AdjListNode *cur_adj_list_node = tree_nodes[cur].list->next;
                    while ( cur_adj_list_node != NULL && cur_adj_list_node->node_content->is_visited )
                        cur_adj_list_node = cur_adj_list_node->next;

                    //No Next Node all nodes used or no node at all
                    
                    if ( cur_adj_list_node != NULL  )
                    {
                        //node nad !visited
                        //Updating the next node
                        tree_nodes[cur].list->next = cur_adj_list_node->next;
                        cur = cur_adj_list_node->node_content->dest;
                        vertices.push(cur);
                    }
                    //There is no node
                    else
                    {
                        tree_nodes[cur].list->next = cur_adj_list_node;
                        if ( !vertices.empty() )
                        {
                            //cur useless
                            vertices.pop();
                            //It doesn't have a parent hence it is 0
                            if ( !vertices.empty() )
                            {
                                int cur_parent=vertices.top();
                                //cout << cur_parent << " parent of " << cur << "\n";

                                update_l1_l2(tree_nodes+cur_parent, max(tree_nodes[cur].len1+1,tree_nodes[cur].len2+1));
                                //update_l1_l2(tree_nodes+cur_parent, tree_nodes[cur].len2+1);
                                cur = cur_parent;
                            }
                        }
                    }
            }while ( !vertices.empty() );
            
            return find_highest();
        }
};
int main()
{
    int T; cin >>T; 
    tree t(T); T--;
    while(T--)
    {
        int a,b;
        cin >> a >> b;
        a--; b--;
        t.add_edge(a,b);
        t.add_edge(b,a);
    }
    cout << t.find_longest_path() <<"\n"; 
    return 0;
}    
