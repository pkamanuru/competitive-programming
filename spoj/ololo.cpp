#include <iostream>
#include <cstdio>
using namespace std;
int main()
{
    int T; scanf("%d",&T);
    int answer=0;
    while (T--)
    {
        int n; scanf("%d",&n);
        answer ^= n;
    }
    printf("%d\n",answer);
    return 0;
}    
