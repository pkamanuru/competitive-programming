#include <iostream>
#include <vector>
#include <utility>
using namespace std;
typedef pair <int,int> direction;
char a[101][101];
int answer_a[101][101];
int N,M;
direction get_pair(direction old_pair , direction d)
{
        return make_pair(old_pair.first + d.first,old_pair.second + d.second);
}
bool if_pair_valid(direction pair)
{
        if ( pair.first >= 0 && pair.first <N && pair.second >= 0 && pair.second < M )
                return true;
        else
                return false;
}
void print_answer()
{
        for ( int i=0; i<N; i++ )
        {
                for( int j=0; j<M; j++ )
                {
                        if ( answer_a[i][j] == -1 )
                                cout << '*';
                        else
                                cout << answer_a[i][j];

                }
                cout << "\n";
        }
}

int main()
{
        vector <direction> directions;
        directions.push_back(make_pair(-1,1));
        directions.push_back(make_pair(0,1));
        directions.push_back(make_pair(1,1));
        directions.push_back(make_pair(1,0));
        directions.push_back(make_pair(1,-1));
        directions.push_back(make_pair(0,-1));
        directions.push_back(make_pair(-1,-1));
        directions.push_back(make_pair(-1,0));
        int P; cin >> P;
        int sno=1;
        while(P--)
        {
                 cin >> N >> M;

                if ( N == 0 && M == 0  )
                        break;
                else
                {
                        for ( int i=0; i<N; i++ )
                                for ( int j=0; j<M; j++ )
                                {
                                        cin >> a[i][j];
                                        if ( a[i][j] == '*' )
                                                answer_a[i][j] = -1;
                                        else
                                                answer_a[i][j] = 0;
                                }
                        //Iterate to find all the numbers
                        
                        for ( int i=0; i<N; i++ )
                                for ( int j=0; j<M; j++ )
                                {
                                        if ( answer_a[i][j] == -1 )
                                                continue;
                                        for ( int k=0; k<8; k++ )
                                        {
                                                direction  new_pair = get_pair(make_pair(i,j),directions.at(k)); 
                                                if ( if_pair_valid(new_pair) && answer_a[new_pair.first][new_pair.second] == -1 )
                                                        answer_a[i][j]++;
                                        }
                                }
                }
                cout <<"Case " << sno++ << ":\n";
                print_answer();
        }
        return 0;
}        
