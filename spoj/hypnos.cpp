#include <iostream>
using namespace std;
int main()
{
    bool is_number[811];
    fill_n(is_number , 811 , false);
    long long N; cin >> N;
    long long temp=0;
    long long process=0;
    while ( 1  )
    {
        while ( N!=0 )
        {
            long long x = N%10;
            temp += x*x;
            N/=10;
        }
        process++;
        if ( temp == 1 )
        {
            cout << process << endl;
            break;
        }
        else
        {
            if ( !is_number[temp] )
                is_number[temp]=true;
            else
            {
                cout << -1 << endl;
                break;

            }
            N=temp;
            temp=0;
        }
    }
   return 0;
}    
