#include <iostream>
#include <cstdio>
#include <algorithm>
#include <stack>
using namespace std;
long long I,J;
long long cache[1000001];
void print_stack(stack <int> s)
{
    cout << "\n------------------------------\n";
    while ( !s.empty() )
    {
        cout << s.top() << endl;
        s.pop();
    }
    cout << "\n------------------------------\n";
}
void print_matrix(int i , int j)
{
        for ( int x=i; x<=j; x++ )
                cout << cache[x] <<":";
        cout << endl;
}
long long max_cycle_length(int no1, int no2)
{
   for ( int i=no1; i<=no2; i++ )
   {
        if ( cache[i] == -1 )
        {
            long long no =i;
            stack <long long> indices;
            indices.push(no);
            while ( no != 1 )
            {
               if ( no < 1000001 && cache[no] != -1)
                   break;
               if ( no % 2 == 0 )   no >>= 1;
               else no = 3*no + 1;
               indices.push(no);
               cout.flush();
            }
            long long cycle = cache[indices.top()];
            indices.pop();
            while( !indices.empty() )
            {
                ++cycle;
                if ( indices.top() < 1000001 )
                    cache[indices.top()] = cycle;
                indices.pop();
            }
        }
   }
   return *max_element(cache+no1,cache+no2+1);
}
int main()
{
    fill_n ( cache , 1000001,-1 );
    cache[0] = 0; cache[1]=1;
    while ( (scanf("%lld",&I)) > 0  && (scanf("%lld",&J)) > 0) 
    {
        long long ix=I,jx=J;
        if ( I > J )
            swap(I,J);
        cout << ix << " " << jx << " " << max_cycle_length(I,J) << "\n";  
    }
    return 0;
}    
