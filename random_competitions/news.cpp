#include <iostream>
#include <vector>
using namespace std;
int main()
{
    int N,K,T;
    cin >> N >> K >> T;
    int no_of_times=0;
    vector<int> communicating_people;
    communicating_people.push_back(0);
    int left[N];
    fill_n(left, N, K);
    int added_until=0;
    int N_copy = N;
    N--;
    while ( N > 0 )
    {
        int no_added=0;
        no_of_times++;
        for ( int i=0; i<communicating_people.size(); i++ )
        {
            if (left[communicating_people.at(i)] > 0)
            {
                N--;
                left[communicating_people.at(i)]--;
                no_added++;
            }
        }
        //cout << "Hello" << N <<":"<< added_until<< ":" << N << endl;

        while ( added_until < N_copy && no_added > 0 )
        {
            no_added--;
            added_until++;
            communicating_people.push_back(added_until);
        }
    }
    cout << no_of_times*T << endl;
    return 0;
}    
