#include <iostream>
using namespace std;
int main()
{
    int N; //price per chocolate 
    int M; // wrappers per chocolate
    int P; //No of friends
    cin >> N >> M >> P;
    while ( P-- ) 
    {
        int Q; cin >> Q;
        int wrappers = 0;
        int no_of_chocolates=0;
        {
            while ( Q >= N )
            {
                no_of_chocolates++;
                wrappers++;
                Q -= N;
            }
            while ( wrappers >= M )
            {
                wrappers -= M;
                wrappers++;
                no_of_chocolates++;
            }
            cout << no_of_chocolates << endl;
        }   
    }
    return 0;
}    
