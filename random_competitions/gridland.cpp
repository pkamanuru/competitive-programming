#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <climits>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
/*
 *  * Complete the function below.
 *   */
vector <string> get_all_ordered_paths (int X , int Y , vector <string> result ,string path)
{
    if ( X == 0 && Y == 0 )
    {
        result.push_back(path);
        return result;
    }
    if ( X > 0 )
        result = get_all_ordered_paths(X-1,Y,result,path+"H");
    if ( Y > 0 )
        result = get_all_ordered_paths(X,Y-1,result,path+"V");
    return result;
}
vector <string> get_input(string X)
{
    vector <string > answer;
    string a;
    for ( int i=0; i<X.length(); i++ )
    {
       if ( X.at(i) != ' ' )
           a += X.at(i);
       else
       {
           answer.push_back(a);
           a="";
       }
    }
    answer.push_back(a);
    return answer;
}
vector < string > gridLand(vector < string > inp) 
{
    vector <string> answer;
    for ( int i=0; i<inp.size(); i++ )
    {
        vector<string> cur = get_input(inp.at(i));
        int X = atoi(cur.at(0).c_str());
        int Y = atoi(cur.at(1).c_str());
        int k = atoi(cur.at(2).c_str());
       // cout << "X:" << X << "Y:"<< Y << "K:" << k <<endl;
        vector <string> empty;
        vector <string> a = get_all_ordered_paths(X,Y,empty,"");
        sort(a.begin(),a.end());
        answer.push_back(a.at(k));
    }
    return answer;
}

int main() {
    ofstream fout(getenv("OUTPUT_PATH"));
    vector < string > res;

    int _inp_size;
    cin >> _inp_size;
    cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n'); 
    vector<string> _inp;
    string _inp_item;
    for(int _inp_i=0; _inp_i<_inp_size; _inp_i++) {
        getline(cin, _inp_item);
        _inp.push_back(_inp_item);

    }

    res = gridLand(_inp);
    for(int res_i=0; res_i < res.size(); res_i++) {
        cout << res[res_i] << endl;;

    }

    fout.close();
    return 0;

}
