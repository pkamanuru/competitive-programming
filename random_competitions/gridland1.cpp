#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <climits>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
int bit_representation[20];
/*
 *  * Complete the function below.
 *   */
void add_one(int x)
{
    for ( int i=x-1; i>=0; i-- )
    {
        if ( bit_representation[i] == 1 )
        {
            bit_representation[i] = 0;
            continue;
        }
        else
        {
            bit_representation[i]=1;
            break;
        }
    }
}

vector <string> get_input(string X)
{
    vector <string > answer;
    string a;
    for ( int i=0; i<X.length(); i++ )
    {
       if ( X.at(i) != ' ' )
           a += X.at(i);
       else
       {
           answer.push_back(a);
           a="";
       }
    }
    answer.push_back(a);
    return answer;
}
vector < string > gridLand(vector < string > inp) 
{
    //cout << "Hi";
    vector <string> answer;
    for ( int i=0; i<inp.size(); i++ )
    {
        vector<string> cur = get_input(inp.at(i));
        int X = atoi(cur.at(0).c_str());
        int Y = atoi(cur.at(1).c_str());
        int k = atoi(cur.at(2).c_str());
        int size=-1;
        int op_size = X+Y;
        fill_n(bit_representation,20,0);
        for ( int z=0; z<pow(2,X+Y); z++ )
        {
            //cout << "hi";
           add_one(X+Y); 
           int x_count=0;
           int y_count=0;
           for ( int j=0; j<op_size; j++ )
           {
               if ( bit_representation[j] == 0 )
                   x_count++;
               if ( bit_representation[j] == 1 )
                   y_count++;

           }
           if ( x_count == X && y_count == Y )
               size++;
           if ( size == k )
           {
               string gen= "";
               for ( int j=0; j<op_size; j++ )
               {
                   if ( bit_representation[j] == 0 )
                       gen += "H";
                   else
                       gen += "V";

               }
               answer.push_back(gen);
               break;
           }
        }
    }
    return answer;
}

int main() {
    ofstream fout(getenv("OUTPUT_PATH"));
    vector < string > res;

    int _inp_size;
    cin >> _inp_size;
    cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n'); 
    vector<string> _inp;
    string _inp_item;
    for(int _inp_i=0; _inp_i<_inp_size; _inp_i++) {
        getline(cin, _inp_item);
        _inp.push_back(_inp_item);

    }

    res = gridLand(_inp);
    for(int res_i=0; res_i < res.size(); res_i++) {
        cout << res[res_i] << endl;;

    }

    fout.close();
    return 0;

}
