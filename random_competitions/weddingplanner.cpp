#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <climits>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
float round_up(double x)
{
    double y = 100 * x;
    int rounded = (int)(y + 0.5);
    return float(rounded)/float(100);
}
/*float round_up(const double x) {
    int decDigits=2;
    stringstream ss;
    ss << fixed;
    ss.precision(decDigits); // set # places after decimal
    ss << x;
    return stof(ss.str());
}*/
/*float round_up(float val)
{
    float rounded_up = ceilf(val * 100) / 100;  
    return rounded_up;
}*/

int find_prev( int n , vector <string> ucost )
{
    for ( int i=n-1; i>=0; i-- )
    {
        if ( ucost.at(i).at(0) != '-' || ucost.at(i) != "0")
        {
            return i;
        }
    }
    return n;
}


/*
 * Complete the function below.
 */
int find_next( int n , vector <string> ucost )
{
    for ( int i=n+1; i<ucost.size(); i++ )
    {
        if ( ucost.at(i).at(0) != '-' || ucost.at(i) != "0")
        {
            return i;
        }
    }
    return n;
}

float Interpolate(int n, vector < int > amount, vector < string > ucost) 
{
    int size=0,p=0;
    for ( int i=0; i<ucost.size(); i++ )
    {
        //cout << ucost.at(i) << ":" << ucost.at(i) << endl;
        if ( ucost.at(i).at(0) != '-' && ucost.at(i) != "0")
        {
           size++;
           p=i;
        }
    }
    //cout << size << "size" << endl;
    if ( size == 1 )
        return stof(ucost.at(p));
    //if ( ucost.size() == 1 )
     //   return stof(ucost.at(0));

    int pos=-1;
    for ( int i=0; i<amount.size(); i++ )
    {
        if ( ucost.at(i).at(0) != '-' && ucost.at(i) != "0")
            pos = i;
        if ( amount.at(i) == n )
        {
            return stof(ucost.at(i));
        }
        else if ( amount.at(i) > n )
        {
            if ( i == 0 )
            {
                //cout << i << "amount i first element " << amount.at(i) << endl;
                int j=find_next(i,ucost);
                if ( j == i )
                    return stof(ucost.at(i));
                else
                {
                    float l = stof(ucost.at(i));
                    float h = stof(ucost.at(j));
                    float per_unit_decrease = (l-h)/(amount.at(j) - amount.at(i));
                    int l_no_of_units = amount.at(i)-n;
                    int h_no_of_units = amount.at(j)-n;
                    float final_price = min((l+(l_no_of_units*per_unit_decrease)),(h+(h_no_of_units*per_unit_decrease)));
                    return round(final_price);
                }
            }
                    else
            { 
                //cout << "i in between" << endl;
                int j = find_prev(i,ucost);
                swap(i,j);
                if ( j == i )
                    return stof(ucost.at(i));
                else
                {
                    float l = stof(ucost.at(i));
                    float h = stof(ucost.at(j));
                    float per_unit_decrease = (l-h)/(amount.at(j) - amount.at(i));
                    int l_no_of_units = amount.at(i)-n;
                    int h_no_of_units = amount.at(j)-n;
                    float final_price = min((l+(l_no_of_units*per_unit_decrease)),(h+(h_no_of_units*per_unit_decrease)));
                    return round_up(final_price);

                }

            }
        }
    }    if ( pos == amount.size()-1 )
            {
                int i=pos;
                //cout << i << "amount i == last element" << endl;
                int j = find_prev(i,ucost);
                swap(i,j);
                if ( j == i )
                    return stof(ucost.at(i));
                else
                {
                    float l = stof(ucost.at(i));
                    float h = stof(ucost.at(j));
                    float per_unit_decrease = (l-h)/(amount.at(j) - amount.at(i));
                    int l_no_of_units = amount.at(i)-n;
                    int h_no_of_units = amount.at(j)-n;
                    float final_price = min((l+(l_no_of_units*per_unit_decrease)),(h+(h_no_of_units*per_unit_decrease)));
                    return round_up(final_price);
                }
            }

    return stof(ucost.at(pos));
}



int main() {
    //ofstream (getenv("OUTPUT_PATH"));
    float res;
    int _n;
    cin >> _n;
    cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n');
    
    
    int _amount_size;
    cin >> _amount_size;
    cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n'); 
    vector<int> _amount;
    int _amount_item;
    for(int _amount_i=0; _amount_i<_amount_size; _amount_i++) {
        cin >> _amount_item;
        cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n');
        _amount.push_back(_amount_item);
    }
    
    
    int _ucost_size;
    cin >> _ucost_size;
    cin.ignore (std::numeric_limits<std::streamsize>::max(), '\n'); 
    vector<string> _ucost;
    string _ucost_item;
    for(int _ucost_i=0; _ucost_i<_ucost_size; _ucost_i++) {
        getline(cin, _ucost_item);
        _ucost.push_back(_ucost_item);
    }
    
    res = Interpolate(_n, _amount, _ucost);
    cout << res << endl;
    
    //fout.close();
    return 0;
}

