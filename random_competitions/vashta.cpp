#include <iostream>
using namespace std;
int matrix[10000][10000];
int i1,j1,i2,j2;
bool is_valid ( int i , int j )
{
    if ( i >= i1 && i <= i2 && j >= j1 && j<=j2 )
        return false;
    else
        return true;
}
int main()
{
    int R; int C;
    cin >> R >> C;
    for ( int i=0; i<R; i++ )
    {
        for ( int j=0; j<C; j++ )
        {
            cin >> matrix[i][j];
        }
    }

    int N;
    cin >> N;

    while ( N-- )
    {
        int max = 0; int ia=0, ja=0;
        cin >> i1 >> j1 >> i2 >> j2;
        for ( int i=0; i<R; i++ )
        {
            for ( int j=0; j<C; j++  )
            {
                if (is_valid(i,j) &&  matrix[i][j] >= max )
                {
                    max = matrix[i][j];
                    ia = i;
                    ja = j;
                }
            }
        }
        cout << ia << " " << ja << endl;
    }

    return 0;
}    


