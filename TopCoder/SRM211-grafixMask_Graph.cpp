#include <cstdio>
#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <set>
#include <stack>
#include <vector>
#include <sstream>
#include <typeinfo>
#include <fstream>

using namespace std;

class grafixMask {
public:
  const static size_t maxRow = 400;
  const static size_t maxCol = 600;
  bool pixelGrid[maxRow][maxCol];

  void fill(int *indices) {
    size_t rowTop = indices[0], rowBottom = indices[2],
      colLeft = indices[1], colRight = indices[3];

    for (int i=rowTop; i<=rowBottom; i++) {
      for(int j=colLeft; j<=colRight; j++) {
        pixelGrid[i][j] = true;
      }
    }
  }

  void fillGrid(string& rectangle) {
    std::string delim = " ";
    size_t noOfCords = 4;
    int indices[noOfCords];
    auto start = decltype(std::string::npos){0};
    for(int i=0; i<noOfCords; i++) {
      auto end = rectangle.find(delim, start);
      if (end == std::string::npos) {
        end = rectangle.length();
      }
        //Handle Invalid Input

        string val = rectangle.substr(start, end - start);
        indices[i] = stoi(val);
        start = end + delim.length();
    }

    fill(indices);
  }

  bool isValid(int i, int j) {
    if(i >= 0 && i < maxRow && j >=0 && j < maxCol && !pixelGrid[i][j]) {
      return true;
    } else {
      return false;
    }
  }

  vector<pair<int, int>> getAllDirections(int i, int j) {
    vector<pair<int,int>> directions = {
      make_pair(-1, 0),
      make_pair(1, 0),
      make_pair(0, 1),
      make_pair(0, -1)
    };

    vector<pair<int, int>> answer;
    for(auto& dir : directions) {
      int x = dir.first, y = dir.second;
        if ( isValid(x+i, y+j) ) {
          answer.push_back(make_pair(x+i, y+j));
        }
    }
    return answer;
  }

  int dfs(int i, int j) {
    int area = 0;

    stack<pair<int, int>> dfsStack;
    dfsStack.push(make_pair(i,j));

    while(!dfsStack.empty()) {
      pair<int, int> stackTop = dfsStack.top();
      dfsStack.pop();

      if(pixelGrid[stackTop.first][stackTop.second])
        continue;

      pixelGrid[stackTop.first][stackTop.second] = true;
      area += 1;

      vector<pair<int, int>> directions = getAllDirections(stackTop.first, stackTop.second);

      for(auto& direction : directions) {
        dfsStack.push(direction);
      }
    }

    return area;
  }

  vector<int> sortedAreas(vector<string> rectangles) {
    size_t rowTop = 0, rowBottom = 399, colLeft = 0, colRight = 599;
    fill_n(&pixelGrid[0][0], maxRow*maxCol, false);
    vector<int> areas;

    for(auto& rectangle : rectangles) {
      fillGrid(rectangle);
    }

    for(int i=rowTop; i<=rowBottom; i++) {
      for(int j=colLeft; j<=colRight; j++) {
        if(!pixelGrid[i][j]) {
          areas.push_back(dfs(i, j));
        }
      }
    }
    sort(areas.begin(), areas.end());
    return areas;
  }
};

// CUT begin
ifstream data("grafixMask.sample");

string next_line() {
    string s;
    getline(data, s);
    return s;
}

template <typename T> void from_stream(T &t) {
    stringstream ss(next_line());
    ss >> t;
}

void from_stream(string &s) {
    s = next_line();
}

template <typename T> void from_stream(vector<T> &ts) {
    int len;
    from_stream(len);
    ts.clear();
    for (int i = 0; i < len; ++i) {
        T t;
        from_stream(t);
        ts.push_back(t);
    }
}

template <typename T>
string to_string(T t) {
    stringstream s;
    s << t;
    return s.str();
}

string to_string(string t) {
    return "\"" + t + "\"";
}

template <typename T> string to_string(vector<T> ts) {
    stringstream s;
    s << "[ ";
    for (int i = 0; i < ts.size(); ++i) {
        if (i > 0) s << ", ";
        s << to_string(ts[i]);
    }
    s << " ]";
    return s.str();
}

bool do_test(vector<string> rectangles, vector<int> __expected) {
    time_t startClock = clock();
    grafixMask *instance = new grafixMask();
    vector<int> __result = instance->sortedAreas(rectangles);
    double elapsed = (double)(clock() - startClock) / CLOCKS_PER_SEC;
    delete instance;

    if (__result == __expected) {
        cout << "PASSED!" << " (" << elapsed << " seconds)" << endl;
        return true;
    }
    else {
        cout << "FAILED!" << " (" << elapsed << " seconds)" << endl;
        cout << "           Expected: " << to_string(__expected) << endl;
        cout << "           Received: " << to_string(__result) << endl;
        return false;
    }
}

int run_test(bool mainProcess, const set<int> &case_set, const string command) {
    int cases = 0, passed = 0;
    while (true) {
        if (next_line().find("--") != 0)
            break;
        vector<string> rectangles;
        from_stream(rectangles);
        next_line();
        vector<int> __answer;
        from_stream(__answer);

        cases++;
        if (case_set.size() > 0 && case_set.find(cases - 1) == case_set.end())
            continue;

        cout << "  Testcase #" << cases - 1 << " ... ";
        if ( do_test(rectangles, __answer)) {
            passed++;
        }
    }
    if (mainProcess) {
        cout << endl << "Passed : " << passed << "/" << cases << " cases" << endl;
        int T = time(NULL) - 1472118723;
        double PT = T / 60.0, TT = 75.0;
        cout << "Time   : " << T / 60 << " minutes " << T % 60 << " secs" << endl;
        cout << "Score  : " << 500 * (0.3 + (0.7 * TT * TT) / (10.0 * PT * PT + TT * TT)) << " points" << endl;
    }
    return 0;
}

int main(int argc, char *argv[]) {
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);
    set<int> cases;
    bool mainProcess = true;
    for (int i = 1; i < argc; ++i) {
        if ( string(argv[i]) == "-") {
            mainProcess = false;
        } else {
            cases.insert(atoi(argv[i]));
        }
    }
    if (mainProcess) {
        cout << "grafixMask (500 Points)" << endl << endl;
    }
    return run_test(mainProcess, cases, argv[0]);
}
// CUT end
