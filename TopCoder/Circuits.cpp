#include <cstdio>
#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <set>
#include <vector>
#include <sstream>
#include <typeinfo>
#include <fstream>

using namespace std;

struct adjListNode {
  struct adjListNode *next;
  struct vertex *nodeVertex;
  int weight;
  bool visited;
};

struct vertex {
  struct adjListNode *adjListHead;
  struct adjListNode *adjListTail;
  int longestPath;
  int vertexNo;
  bool visited;
};

class Circuits {
public:
  Circuits() {
    vertices = NULL;
    noOfVertices = 0;
  }
  ~Circuits() {
    if( vertices != NULL) {
      delete(vertices);
    }
    }

  struct vertex *vertices;
  int noOfVertices;

  struct adjListNode *buildAdjListNode(int vertexNumber, int weight) {
    struct adjListNode *newNode = new adjListNode;
    newNode->next = NULL;
    newNode->nodeVertex = &vertices[vertexNumber];
    newNode->weight = weight;
    newNode->visited = false;
    return newNode;
  }
  void initializeVertices() {
    // Initialize Vertices
    for(int i=0; i<noOfVertices; i++) {
      vertices[i].adjListHead = NULL;
      vertices[i].adjListTail = NULL;
      vertices[i].longestPath = 0;
      vertices[i].visited = false;
      vertices[i].vertexNo = i;
    }
  }

  //Adds New Node to a Linkedlist
  void addNodeToVertex(struct vertex *curVertex, struct adjListNode *newNode) {
    if(curVertex->adjListHead == NULL) {
      curVertex->adjListHead = newNode;
      curVertex->adjListTail = curVertex->adjListHead;
    } else {
      curVertex->adjListTail->next = newNode;
      curVertex->adjListTail = newNode;
    }
  }

  void buildGraph(vector<string> &connects, vector<string> &costs) {
    //Build Graph
    //connects describes the vertices each vertex is connected to
    for(int vertexNumber = 0; vertexNumber < noOfVertices; vertexNumber++) {
      istringstream ss(connects[vertexNumber]); //ss << connects[vertexNumber];
      istringstream ss1(costs[vertexNumber]); //ss1 << costs[vertexNumber];
      struct vertex *curVertex = &vertices[vertexNumber];

      int edgeVertex, edgeWeight;
      while((ss >> edgeVertex) > 0 && (ss1 >> edgeWeight) > 0) {
        //Build New AdjListNode
        struct adjListNode *newTail = buildAdjListNode(edgeVertex, edgeWeight);
        addNodeToVertex(curVertex, newTail);
      }
    }
  }

  void dfs(struct vertex *root) {
    if(root->adjListHead == NULL || root->visited) {
      return;
    }

    struct adjListNode *cur = root->adjListHead;
    while(cur != NULL && !cur->visited) {
      //update head
      if(cur->next != NULL) {
        root->adjListHead = cur->next;

        //update tail
        root->adjListTail->next = cur;
        root->adjListTail = cur;
        cur->next = NULL;
      }

      cur->visited = true;
      dfs(cur->nodeVertex);

      cur = root->adjListHead;
    }

    cur = root->adjListHead;
    while(cur != NULL) {
      root->longestPath = max(root->longestPath, cur->nodeVertex->longestPath + cur->weight);
      cur = cur->next;
    }
    cout << "Length:" <<  root->longestPath << "\n";
  }

  int howLong(vector<string> connects, vector<string> costs) {
    //Initialize private members
    noOfVertices = connects.size();
    vertices = new vertex[noOfVertices];

    initializeVertices();
    buildGraph(connects, costs);

    cout << "Graph\n";
    for(int i=0; i<noOfVertices; i++) {
      cout << i << "->";
      struct adjListNode *cur = vertices[i].adjListHead;
      while(cur != NULL) {
        cout << cur->nodeVertex->vertexNo << "(" << cur->weight << ")"<<"->";
        cur = cur->next;
      }
      cout << "\n";

    }
    cout << "Graph\n";

    int longestPath = 0;
    //Iterate over Vertices
    for(int i=0; i<noOfVertices; i++) {
      dfs(&vertices[i]);
      longestPath = max(longestPath, vertices[i].longestPath);
      cout << i << ":" << longestPath << endl;
    }

    return longestPath;
  }
};

// CUT begin
ifstream data("Circuits.sample");

string next_line() {
    string s;
    getline(data, s);
    return s;
}

template <typename T> void from_stream(T &t) {
    stringstream ss(next_line());
    ss >> t;
}

void from_stream(string &s) {
    s = next_line();
}

template <typename T> void from_stream(vector<T> &ts) {
    int len;
    from_stream(len);
    ts.clear();
    for (int i = 0; i < len; ++i) {
        T t;
        from_stream(t);
        ts.push_back(t);
    }
}

template <typename T>
string to_string(T t) {
    stringstream s;
    s << t;
    return s.str();
}

string to_string(string t) {
    return "\"" + t + "\"";
}

bool do_test(vector<string> connects, vector<string> costs, int __expected) {
    time_t startClock = clock();
    Circuits *instance = new Circuits();
    int __result = instance->howLong(connects, costs);
    double elapsed = (double)(clock() - startClock) / CLOCKS_PER_SEC;
    delete instance;

    if (__result == __expected) {
        cout << "PASSED!" << " (" << elapsed << " seconds)" << endl;
        return true;
    }
    else {
        cout << "FAILED!" << " (" << elapsed << " seconds)" << endl;
        cout << "           Expected: " << to_string(__expected) << endl;
        cout << "           Received: " << to_string(__result) << endl;
        return false;
    }
}

int run_test(bool mainProcess, const set<int> &case_set, const string command) {
    int cases = 0, passed = 0;
    while (true) {
        if (next_line().find("--") != 0)
            break;
        vector<string> connects;
        from_stream(connects);
        vector<string> costs;
        from_stream(costs);
        next_line();
        int __answer;
        from_stream(__answer);

        cases++;
        if (case_set.size() > 0 && case_set.find(cases - 1) == case_set.end())
            continue;

        cout << "  Testcase #" << cases - 1 << " ... ";
        if ( do_test(connects, costs, __answer)) {
            passed++;
        }
    }
    if (mainProcess) {
        cout << endl << "Passed : " << passed << "/" << cases << " cases" << endl;
        int T = time(NULL) - 1472198689;
        double PT = T / 60.0, TT = 75.0;
        cout << "Time   : " << T / 60 << " minutes " << T % 60 << " secs" << endl;
        cout << "Score  : " << 275 * (0.3 + (0.7 * TT * TT) / (10.0 * PT * PT + TT * TT)) << " points" << endl;
    }
    return 0;
}

int main(int argc, char *argv[]) {
    cout.setf(ios::fixed, ios::floatfield);
    cout.precision(2);
    set<int> cases;
    bool mainProcess = true;
    for (int i = 1; i < argc; ++i) {
        if ( string(argv[i]) == "-") {
            mainProcess = false;
        } else {
            cases.insert(atoi(argv[i]));
        }
    }
    if (mainProcess) {
        cout << "Circuits (275 Points)" << endl << endl;
    }
    return run_test(mainProcess, cases, argv[0]);
}
// CUT end
