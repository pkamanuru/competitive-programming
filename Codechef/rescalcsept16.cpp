#include <iostream>
#include <vector>

using namespace std;
int main(int argc, char *argv[]) {
  int T; cin >> T;
  for(int testCase=0; testCase < T; testCase++) {
    int noOfPlayers; cin >> noOfPlayers;
    vector<int> winningPlayers;
    int maxScore = 0;

    //Evaluating Each player's performance
    for(int player=1; player<=noOfPlayers; player++) {
      int noOfCookies=0; cin >> noOfCookies;
      // 0 Based Types
      int noOfCookiesOfType[6];
      fill_n(noOfCookiesOfType, 6, 0);
      for(int cookie=0; cookie<noOfCookies; cookie++) {
        int type; cin >> type;
        //Making it 0 Based Type
        type--;
        noOfCookiesOfType[type]++;
      }


      //Calculating Score for Player
      int curScore = noOfCookies;
      bool cookiesOver = false;
      while(!cookiesOver) {
        vector<int> distinctTypes;
        for(int type=0; type<6; type++) {
          if(noOfCookiesOfType[type] > 0) {
            distinctTypes.push_back(type);
          }
        }

        if(distinctTypes.size() < 4) {
          cookiesOver = true;
        } else {
          int boxSize = distinctTypes.size();

          for(auto &type : distinctTypes) {
            noOfCookiesOfType[type] -= 1;
          }

          if(boxSize == 4) {
            curScore +=1;
          } else if(boxSize == 5) {
            curScore += 2;
          } else if(boxSize == 6){
            curScore += 4;
          }
        }
      }

      // If player fit to be winner
      if(curScore == maxScore) {
        winningPlayers.push_back(player);
      } else if(curScore > maxScore) {
        winningPlayers.clear();
        winningPlayers.push_back(player);
        maxScore = curScore;
      }
      // Other Score don't care about the player
    }

    if(winningPlayers.size() > 1) {
      cout << "tie\n";
    } else if(winningPlayers.size() == 1) {
      if(winningPlayers[0] == 1) {
        cout << "chef\n";
      } else {
        cout << winningPlayers[0] << "\n";
      }
    }
  }
  return 0;
}
