#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <algorithm>
#include <iterator>

using namespace std;
set<int> getNonConnectedVertices(set<int> &A, vector<vector<bool>> &hasEdge, vector<bool> &assignedTable) {
  set<int> disConnectedVertices;
  for(auto &a : A) {
    for(int i=0; i<hasEdge.size(); i++) {
      if( !hasEdge[i][a] && !assignedTable[i] ) {
        disConnectedVertices.insert(i);
      }
    }
  }
  return disConnectedVertices;
}

bool setsCompatible(set<int>& A, set<int>& B, vector<vector<bool>>& hasEdge) {
  for(auto &a : A) {
    for(auto &b : B) {
      if(!hasEdge[a][b]) {
        return false;
      }
    }
  }
  return true;
}

bool setCompatible(set<int>& A, vector<vector<bool>> & hasEdge) {
  return setsCompatible(A, A, hasEdge);
}

void addSetToSet(set<int> &A, set<int> &B, vector<bool> &assignedTable) {
  for(auto &a : A) {
    B.insert(a);
    assignedTable[a] = true;
  }
}

void printSet(set<int> &A) {
  for(auto &a : A) {
    cout << a << ":";
  }
  cout << endl;
}

bool handleVertex(int vertex, vector<vector<bool>> &hasEdge, vector<bool> &assignedTable) {
  set<int> set1;
  set<int> set2;
  set<int> handleSet;
  bool insertSet1 = true;
  handleSet.insert(vertex);

  while(!handleSet.empty()) {
    //cout << "HandleSet:::";
    //printSet(handleSet);
    set<int> *mainSet;
    if(insertSet1) {
      mainSet = &set1;
    } else {
      mainSet = &set2;
    }

    if(!setCompatible(handleSet, hasEdge) || !setsCompatible(handleSet, *mainSet, hasEdge)) {
      return false;
    }
    addSetToSet(handleSet, *mainSet, assignedTable);
    handleSet = getNonConnectedVertices(handleSet, hasEdge, assignedTable);
    insertSet1 = !insertSet1;
  }
  return true;
}

int main(int argc, char *argv[]) {
  int T; cin >> T;
  while(T--) {
    int noOfFriends; cin >> noOfFriends;
    int noOfEdges; cin >> noOfEdges;

    vector<vector<bool>> hasEdge(noOfFriends, vector<bool>(noOfFriends));
    vector<bool> assignedTable(noOfFriends);
    for(int i=0; i<noOfFriends; i++) {
      assignedTable[i] = false;
      for(int j=0; j<noOfFriends; j++) {
        hasEdge[i][j] = false;
      }
      hasEdge[i][i] = true;
    }

    for(int edge=0; edge < noOfEdges; edge++) {
      int x,y; cin >> x; cin >> y;
      x--; y--;
      hasEdge[x][y] = true;
      hasEdge[y][x] = true;
    }
    list<int> fullSet;
    list<int> anotherSet;
    bool hasArrangement = true;

    for(int vertex=0; vertex<noOfFriends; vertex++) {
      if( !assignedTable[vertex] && !handleVertex(vertex, hasEdge, assignedTable)) {
        hasArrangement = false;
        break;
      }
    }

    if(hasArrangement) {
      cout << "YES\n";
    } else {
      cout << "NO\n";
    }
  }
  return 0;
}
