#include <iostream>
#include <cstdio>
using namespace std;
int main()
{
        int T;
        int N,M;
        scanf("%d",&T);
        while (T--)
        {
                scanf("%d %d",&N,&M);
                int x=0;
                if  ( M == 0 )
                {
                        printf("No 1\n");
                        continue;
                }
                else
                        x = N % M;

                int array[M+x];
                for ( int i=0; i<x; i++ )
                        array[i] = M+i;

                for ( int i=x; i<M+x; i++ )
                        array[i] = i-x;

                bool marked_array[M+x];
                fill_n(marked_array,M+x,false);
                int count_M=0;
                int count_x=0;
                int cur=0;
                cout << endl; 
                for ( int i=0; i<M+x; i++ )
                        cout << array[i] << ":";
                cout << endl;
                while ( marked_array[cur] == false )
                {
                        cout << array[cur] << ":";
                        marked_array[cur] = true;
                        if ( cur < M )
                                count_M++;
                        else
                                count_x++;
                        cur = array[cur];
                }
                cout << endl;
                cout << count_M << ":" << count_x << endl;
                if ( count_M + count_x == M+x )
                        printf("Yes\n");
                else
                {
                        int multiple = N/M;
                        int answer = multiple*count_M + count_x;
                        printf("No %d\n",answer);
                }
                        
        }
        return 0;
}        
